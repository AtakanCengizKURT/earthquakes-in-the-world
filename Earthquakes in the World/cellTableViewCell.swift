//
//  cellTableViewCell.swift
//  Earthquakes in the World
//
//  Created by Atakan Cengiz KURT on 30.05.2017.
//  Copyright © 2017 Atakan Cengiz KURT. All rights reserved.
//

import UIKit

class cellTableViewCell: UITableViewCell {

    @IBOutlet weak var magnitudeLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var depthLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    var a = Double(5.1)
    override func awakeFromNib() {
        super.awakeFromNib()
        magnitudeLabel.layer.cornerRadius = magnitudeLabel.frame.size.width / 2
        magnitudeLabel.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
