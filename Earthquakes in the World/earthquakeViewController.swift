//
//  earthquakeViewController.swift
//  Earthquakes in the World
//
//  Created by Atakan Cengiz KURT on 31.05.2017.
//  Copyright © 2017 Atakan Cengiz KURT. All rights reserved.
//

import UIKit

class earthquakeViewController: UIViewController {

    @IBOutlet weak var webView1: UIWebView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let newsUrl = URL(string: earthquakeUrl[myIndex]) //URL tipine dönüştürdük
        let newsRequest = URLRequest(url: newsUrl!)
        webView1.loadRequest(newsRequest) //adresteki veriyi sayfaya yükle
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
