//
//  ViewController.swift
//  Earthquakes in the World
//
//  Created by Atakan Cengiz KURT on 30.05.2017.
//  Copyright © 2017 Atakan Cengiz KURT. All rights reserved.
//

import UIKit

var magnitude = [String]()
var location = [String]()
var depth = [String]()
var time = [String]()
var date = [String]()
var date_time = [String]()
var earthquakeUrl = [String]()
var myIndex = 0
var menuShowing = false
class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView1: UITableView!
    @IBOutlet weak var viewMenu: UIView!
    @IBOutlet weak var viewMenuLeading: NSLayoutConstraint!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewMenu.layer.shadowOpacity = 1
        viewMenu.layer.shadowRadius = 6
        
        let url = URL(string: "http://tr.earthquake-report.com/feeds/recent-eq?json")
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error != nil {//Hata varsa
                print(error!)
            }else{
                do{
                    let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSArray
                    
                    if let jsonDic = json{
                        for i in 0..<jsonDic.count{
                            if let jsonArray = jsonDic[i] as? NSDictionary{
                                
                                if let magnitudeArray = jsonArray["magnitude"] as? String
                                {
                                    magnitude.append(magnitudeArray)
                                    
                                }
                                if let locationArray = jsonArray["location"] as? String
                                {
                                    location.append(locationArray)
                                }
                                if let depthArray = jsonArray["depth"] as? String
                                {
                                    depth.append(depthArray)
                                }
                                if let date_timeArray = jsonArray["date_time"] as? String
                                {
                                    date_time = date_timeArray.components(separatedBy: "T")
                                    date.append(date_time[0])
                                    time.append(date_time[1])
                                }
                                if let earthquakeUrls = jsonArray["link"] as? String
                                {
                                    earthquakeUrl.append(earthquakeUrls)
                                }
                     
                            }
                        }
                       
                    }
                    
                    DispatchQueue.main.sync {
                        self.tableView1.reloadData()
                        
                    }
                    
                }
                catch{
                    print(error)
                }
            }
        }
        task.resume()

        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    
    //TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return magnitude.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! cellTableViewCell
        
        
        cell.magnitudeLabel.text = magnitude[indexPath.row].description
        
        switch (cell.magnitudeLabel.text!) {
        case "1","1.1","1.2","1.3","1.4","1.5","1.6","1.7","1.8","1.9","2","2.1","2.2","2.3","2.4","2.5","2.6","2.7","2.8","2.9":
            cell.magnitudeLabel.backgroundColor = UIColor.green
        
        case "3","3.1","3.2","3.3","3.4","3.5","3.6","3.7","3.8","3.9","4","4.1","4.2","4.3","4.4","4.5":
            cell.magnitudeLabel.backgroundColor = UIColor.yellow
            case "4.6","4.7","4.8","4.9","5":
            cell.magnitudeLabel.backgroundColor = UIColor.orange
            
        default:
            cell.magnitudeLabel.backgroundColor = UIColor.red
        }
        
        
        cell.locationLabel.text = location[indexPath.row]
        cell.depthLabel.text = depth[indexPath.row]
        cell.timeLabel.text = time[indexPath.row]
        cell.dateLabel.text = date[indexPath.row]
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        myIndex = indexPath.row
        performSegue(withIdentifier: "segue", sender: self)
    }
    
    @IBAction func menu(_ sender: Any) {
        if menuShowing {
            viewMenuLeading.constant = -210
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        }else {
            viewMenuLeading.constant = 0
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        }
        menuShowing = !menuShowing
    }
    
    
    
}

